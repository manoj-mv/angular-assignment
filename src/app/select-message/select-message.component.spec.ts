import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMessageComponent } from './select-message.component';

describe('SelectMessageComponent', () => {
  let component: SelectMessageComponent;
  let fixture: ComponentFixture<SelectMessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectMessageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
