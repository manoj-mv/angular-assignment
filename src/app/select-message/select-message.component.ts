import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';


import * as fromApp from '../store/app.reducer';
import * as customerActions from '../customer/store/customer.actions';
@Component({
  selector: 'app-select-message',
  templateUrl: './select-message.component.html',
  styleUrls: ['./select-message.component.scss']
})
export class SelectMessageComponent implements OnInit,OnDestroy {
  storeSub:Subscription;
  constructor(private store:Store<fromApp.AppState>) { }
  customerCount :number;
  ngOnInit(): void {
    this.storeSub = this.store.select('customer').pipe(
      map(
        customerState =>{
          // console.log(customerState);
          return customerState.customers;
        }
      ),
    ).subscribe(
       customers =>{
        this.customerCount = customers.length;
        console.log('count',this.customerCount);
       }
    )
  }

  onAddClick(){
    console.log('add button clicked')
    this.store.dispatch(new customerActions.AddFormView({showPage:'edit'}));
  }

  ngOnDestroy(){
    if(this.storeSub){
      this.storeSub.unsubscribe();
    }
  }

}
