import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CustomerListComponent } from "../customer/customer-list/customer-list.component";
import { DashboardComponent } from "./dashboard.component";


const routes:Routes = [
    {path:'',component: DashboardComponent,
    children:[
      {path:'',component:CustomerListComponent,outlet:'routerOutlet1'},
      {path:'customers',redirectTo:'',pathMatch:'full'},
      // {path:'customers',component:CustomerListComponent,outlet:'routerOutlet1'},
      // {path:'customers',component:SelectMessageComponent,pathMatch:'full',outlet:'routerOutlet2'},
      // {path:'',component:SelectMessageComponent,pathMatch:'full',outlet:'routerOutlet2'},
      // {path:'id',component:CustomerDetailComponent,outlet:'routerOutlet2'}
    ]
  },
]
@NgModule({
    imports:[
        RouterModule.forChild(routes)
    ],
    exports:[
        RouterModule
    ]
})
export class DashboardRoutingModule{

}