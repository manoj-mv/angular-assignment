import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CustomerModule } from "../customer/customer.module";
import { SidebarComponent } from "../sidebar/sidebar.component";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardComponent } from "./dashboard.component";

@NgModule({
    declarations:[
        DashboardComponent,
        SidebarComponent,
    ],
    imports:[
        CustomerModule,
        RouterModule,

        // routing
        DashboardRoutingModule
    ],
    exports:[
        DashboardComponent,
        SidebarComponent,
    ]
})
export class DashboardModule{}