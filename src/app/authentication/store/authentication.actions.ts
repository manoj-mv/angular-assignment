import { Action } from "@ngrx/store";
import { Customer } from "src/app/customer/model/customer.model";


export const AUTHENTICATION_START = '[Authentication] Authentication Start';
export const AUTHENTICATION_SUCCESS = '[Authentication] Authentication Success';
export const AUTHENTICATION_FAIL = '[Authentication] Authentication Fail'  


export class AuthenticationStart implements Action{
    readonly type = AUTHENTICATION_START;
    constructor(public payload: {email:string,password:string}){}
}

export class AuthenticationSuccess{
    readonly type = AUTHENTICATION_SUCCESS;
    constructor(public payload: {user:Customer}){}
}

export class AuthenticationFail{
    readonly type = AUTHENTICATION_FAIL;
    constructor(public payload:{errorMsg:string}){}
}

export type AuthActionTypes = AuthenticationStart | AuthenticationSuccess |
                              AuthenticationFail; 