import { Customer } from 'src/app/customer/model/customer.model';
import * as authActions from './authentication.actions';

export interface authState{
    user:Customer;
    AuthError:string;
    loading:boolean
} 

const initialState = {
    user:null,
    AuthError:null,
    loading: null
}

// function to authenticate
function authenticate(payload){
    // console.log('fn',payload)
}

export function AuthenticationReducer(state=initialState,action:authActions.AuthActionTypes){
    switch(action.type){
        case authActions.AUTHENTICATION_START:
            return {
                ...state,
                AuthError:null,
                loading:true
            };
        case authActions.AUTHENTICATION_SUCCESS:
            return {
                ...state,
                user:action.payload.user,
                AuthError:null,
                loading:null
            }
        case authActions.AUTHENTICATION_FAIL:
            return {
                ...state,
                user:null,
                AuthError : action.payload.errorMsg,
                loading:null
            }
        default:
            return state;
    }
}