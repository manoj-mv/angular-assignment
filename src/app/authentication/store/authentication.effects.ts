import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { of, throwError } from "rxjs";
import { dispatch } from "rxjs/internal/observable/pairs";
import { catchError, map, switchMap, take, tap } from "rxjs/operators";
import { Customer } from "src/app/customer/model/customer.model";


import * as fromApp from '../../store/app.reducer';
import * as authActions from './authentication.actions';

@Injectable()
export class AuthenticationEffects{
    constructor(
        private actions$: Actions,
        private store:Store<fromApp.AppState>,
        private router:Router
    ){}
    
    // Effect for authentication
    @Effect()
    AuthLogin = this.actions$.pipe(
        ofType(authActions.AUTHENTICATION_START),
        map(
            (loginActionData:authActions.AuthenticationStart) =>{
               return loginActionData.payload;
            }
        ),
        switchMap(
            payload =>{
                return this.store.select('customer').pipe(
                    take(1),
                    map(
                        customersStoreData=>{
                            // array of customers
                            const customers = customersStoreData.customers;
                            console.log(customers);
                            // authentication check
                            const MatchedUser = customers.find(
                                (customer)=>{
                                    console.log(customer.email);
                                    return (customer.email == payload.email && 
                                           customer.password == payload.password);
                                }
                            )
                            console.log('Authenticated user:',MatchedUser)
                            if(MatchedUser){
                                // this.router.navigate(['/dashboard'])
                                return new authActions.AuthenticationSuccess({user:MatchedUser});
                            }
                            else{
                                return new authActions.AuthenticationFail({errorMsg:'Credentials not found..'});
                                // return Error('Credentials not found..');
                                // of({failed:'failed'});
                            }
                        }
                    ),
                    // catchError(
                    //     error => {
                    //         console.log(error);
                    //         this.store.dispatch(new authActions.AuthenticationFail({errorMsg:error.message}));
                    //     }
                    // )
                )
            }
        )
    )


    // Redirect after successfull login
    @Effect({dispatch:false})
    redirect = this.actions$.pipe(
        ofType(authActions.AUTHENTICATION_SUCCESS),
        tap(
            (authSuccessActionData: authActions.AuthenticationSuccess) =>{
                if(authSuccessActionData.payload.user){
                    this.router.navigate(['/dashboard']);
                }
            }
        )
    )

}

