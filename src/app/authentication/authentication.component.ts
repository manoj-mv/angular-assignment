import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';


import * as fromApp from '../store/app.reducer';
import * as authActions from './store/authentication.actions';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit, OnDestroy {
  storeSubsription :Subscription;
  authError=null;
  constructor(public store:Store<fromApp.AppState>) { }

  ngOnInit(): void {
    this.storeSubsription = this.store.select('authentication')
      .subscribe(
        authStoreData =>{
          // console.log(authStoreData);
          this.authError = authStoreData.AuthError;
          if(this.authError){
            setTimeout(()=>{
              this.authError=null;
            },3000)
          }
        }
      )
  }
  onSubmit(form: NgForm){
    if(!form.valid){
      return;
    }
    console.log(form.value);

    this.store.dispatch(new authActions.AuthenticationStart(form.value));
    
    form.reset();
  }

  ngOnDestroy(){
    if(this.storeSubsription){
      this.storeSubsription.unsubscribe();
    }
  }
}
