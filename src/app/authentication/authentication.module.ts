import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { AuthenticationComponent } from "./authentication.component";

@NgModule({
    declarations:[
        AuthenticationComponent
    ],
    imports:[
        FormsModule,
        CommonModule,
        RouterModule.forChild([{path:'',component:AuthenticationComponent}])
    ]
})
export class AuthenticationModule{}