import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { map, take } from "rxjs/operators";

import * as fromApp from '../store/app.reducer';

@Injectable({
    providedIn:'root'
})
export class AuthenticationGaurd implements CanActivate{
    constructor(
        private store:Store<fromApp.AppState>,
        private router:Router
    ){}

    canActivate(route:ActivatedRouteSnapshot,state:RouterStateSnapshot):
    boolean | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>{
        return this.store.select('authentication').pipe(
            take(1),
            map(
                authenticationState =>{
                    return authenticationState.user;
                }
            ),
            map(
                user =>{
                    const isAuth = user ? true : false;
                    if(isAuth){
                        return true;
                    }
                    else{
                        return this.router.createUrlTree(['/auth']);
                    }
                }
            )
        );
    }
}