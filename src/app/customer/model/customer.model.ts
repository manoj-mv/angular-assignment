export class Customer {
    constructor(
        public fname:string,
        public lname:string,
        public accountNum:string,
        public email:string,
        public password:string,
        public state:string,
        public gender:string,
        public country?:Array<object>
    ){}
}