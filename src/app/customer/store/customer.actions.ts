import { Action } from "@ngrx/store";
import { Customer } from "../model/customer.model";

export const ADD_FORM_VIEW = '[Customer] Add Form View'
export const ADD_CUSTOMER = '[Customer] Add Customer';
export const UPDATE_CUSTOMER = '[Customer] Update Customer';
export const DELETE_CUSTOMER ='[Customer] Delete Customer';
export const SELECTED_CUSTOMER = '[Customer] Selected Customer';
export const GO_TO_DEFAULT = '[Customer] Got To Default Page';

export class AddFormView implements Action{
    readonly type = ADD_FORM_VIEW;
    constructor(public payload:{ showPage:string } ){}
}

export class AddCustomer implements Action{
    readonly type = ADD_CUSTOMER;
    constructor(public payload:{newCustomer: Customer}){}
}

export class SelectedCustomer implements Action{
    readonly type = SELECTED_CUSTOMER;
    constructor(public payload:{user:Customer,id:number,showPage:string}){}
}

export class UpdateCustomer implements Action{
    readonly type= UPDATE_CUSTOMER;
    constructor(public payload:{index:number,editedCustomer: Customer}){}
}

export class DeleteCustomer implements Action{
    readonly type = DELETE_CUSTOMER;
    constructor(public payload:{index:number}){}
}

export class GoToDefault implements Action{
    readonly type = GO_TO_DEFAULT;
}

// Union type
export type CustomerActionTypes = AddCustomer | SelectedCustomer |
                                DeleteCustomer | UpdateCustomer |
                                AddFormView | GoToDefault;