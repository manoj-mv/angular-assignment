
import { Customer } from "../model/customer.model";
import * as customerActions from './customer.actions';
export interface CustomerState{
    customers: Customer[];
    selectedCustomer:Customer;
    selectedCustomerId:number;
    showOnSelect:string;
    editMode:boolean;
} 

const intitialState : CustomerState = {
    customers:[
        {fname:'admin1',lname:'root1',accountNum:'xyz2323445',email:'test@abc.in',password:'test123',state:'kerala',gender:'male'},
        {fname:'admin2',lname:'root2',accountNum:'xyz2323445',email:'test1@abc.in',password:'test123',state:'tamilnadu',gender:'female'},
        {fname:'admin3',lname:'root3',accountNum:'xyz2323445',email:'test2@abc.in',password:'test123',state:'kerala',gender:'other'}
    ],
    selectedCustomer:null,
    showOnSelect:null,
    selectedCustomerId:null,
    editMode:null
}


export function CustomerReducer(state=intitialState,
action: customerActions.CustomerActionTypes){
    switch(action.type){
        case customerActions.ADD_FORM_VIEW:
            console.log('add form action');
            return{
                ...state,
                selectedCustomer:null,
                selectedCustomerId:null,
                showOnSelect:action.payload.showPage,
                editMode:null
            }

        case customerActions.ADD_CUSTOMER:
            console.log('inside add csutomer action');
            let newState ={
                ...state,
                customers:[...state.customers,action.payload.newCustomer],
                selectedCustomer:action.payload.newCustomer,
                selectedCustomerId:null,
                showOnSelect:'view',
                editMode:null
            };
            console.log('new custom',newState);
            return newState;

        case customerActions.SELECTED_CUSTOMER:
            return {
                ...state,
                selectedCustomer:action.payload.user,
                selectedCustomerId:action.payload.id,
                showOnSelect:action.payload.showPage,
                editMode: action.payload.showPage === 'edit' ? true : null
            }
        
        case customerActions.UPDATE_CUSTOMER:
            const updatedCustomer = {
                ...state.customers[action.payload.index],
                ...action.payload.editedCustomer
            }
            const updatedCustomers = [...state.customers];
            updatedCustomers[action.payload.index] = updatedCustomer;
            return {
                ...state,
                customers:updatedCustomers,
                editMode:null,
                selectedCustomer:updatedCustomer,
                showOnSelect:'view'
            }

        case customerActions.DELETE_CUSTOMER:
            return {
                ...state,
                customers: state.customers.filter(
                    (customer,index)=>{
                        return index != action.payload.index;
                    }
                ),
                selectedCustomer:null,
                selectedCustomerId:null,
                showOnSelect:'default',
                editMode:null
            }
        
            case customerActions.GO_TO_DEFAULT:
                return {
                    ...state,
                    showOnSelect:'default'
                }
        default:
            return state;
    }
}