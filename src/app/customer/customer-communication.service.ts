import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject, Subscription } from 'rxjs';

import * as fromApp from  '../store/app.reducer';

@Injectable({
  providedIn: 'root'
})
export class CustomerCommunicationService {
  viewClickSub = new Subject<{event:string,id:number}>();
  viewClickChangeComponentSub = new Subject<string>();

  storeSubscription:Subscription;

  constructor(
    private store:Store<fromApp.AppState>
  ) { }

  getCustomerFromIndex(index:number){
    let selectedUser;
    // this.customerCommun.viewClickSub.next({event:'view',id:index});
    this.storeSubscription = this.store.select('customer')
        .subscribe(
          data=>{
            selectedUser = data.customers[index];
            // console.log('from viewClick fn',selectedUser);
          }
        );
    if(this.storeSubscription){
      this.storeSubscription.unsubscribe();
    }
    return selectedUser;
  }
}
