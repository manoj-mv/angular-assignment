import { AfterViewInit, Component, DoCheck, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import {  map, tap } from "rxjs/operators";

import * as fromApp from '../store/app.reducer';
import { CustomerCommunicationService } from './customer-communication.service';
import { Customer } from './model/customer.model';
import * as customerActions from './store/customer.actions';
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit ,OnDestroy{
  index:number;
  loadedFeature:string ='default' ;
  selectedCustomer:Customer;
  storeSub:Subscription;
  constructor(public store:Store<fromApp.AppState>,
              private customerCommun:CustomerCommunicationService) { }

  ngOnInit(): void {
    // this.communicationSub = this.customerCommun.viewClickSub.subscribe(
    //   payload =>{
    //     console.log(payload);
    //     this.loadedFeature=payload.event;
    //     this.index = payload.id;
    //     console.log(this.loadedFeature,this.index);
    //   }
    // )

    this.storeSub = this.store.select('customer')
      .subscribe(
        customerState =>{
          console.log(customerState);
          if(customerState.selectedCustomer){
            this.selectedCustomer = customerState.selectedCustomer;
            this.index = customerState.selectedCustomerId;
            this.loadedFeature = customerState.showOnSelect;
          }
          else{
            if(customerState.showOnSelect === 'edit'){
              this.loadedFeature='edit';
            }
            else{
              this.loadedFeature = 'default';
            }
            
          }
          // this.selectedCustomer = customerState.selectedCustomer;
          // this.index = customerState.selectedCustomerId;
          // this.loadedFeature = customerState.showOnSelect;
        }
      )
  }

  // ngDoCheck(){
  //   this.customerCommun.viewClickSub.next({event:this.loadedFeature,id:this.index});
  // }

  // ngAfterViewInit(){
  //   this.customerCommun.viewClickSub.next({event:this.loadedFeature,id:this.index});
  // }
  ngOnDestroy(){
    if(this.storeSub){
      this.storeSub.unsubscribe();
    }

  }

}
