import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { SelectMessageComponent } from "../select-message/select-message.component";
import { CustomerDetailComponent } from "./customer-detail/customer-detail.component";
import { CustomerFormComponent } from "./customer-form/customer-form.component";
import { CustomerListItemComponent } from "./customer-list/customer-list-item/customer-list-item.component";
import { CustomerListComponent } from "./customer-list/customer-list.component";
import { CustomerComponent } from "./customer.component";

@NgModule({
    declarations:[
        CustomerComponent, 
        CustomerListComponent,
        CustomerDetailComponent,
        CustomerListItemComponent,
        CustomerFormComponent,
        SelectMessageComponent
    ],
    imports:[
        RouterModule,
        ReactiveFormsModule,
        CommonModule
    ],
    exports:[
        CustomerComponent, 
        CustomerListComponent,
        CustomerDetailComponent,
        CustomerListItemComponent,
        CustomerFormComponent
    ]
})
export class CustomerModule {}