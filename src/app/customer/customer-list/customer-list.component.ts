import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import * as fromApp from '../../store/app.reducer';
import { Customer } from '../model/customer.model';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit,OnDestroy {
  storeSubscription:Subscription;
  customerList:Customer[];
  constructor(
    private store:Store<fromApp.AppState>
  ) { }

  ngOnInit(): void {
    this.storeSubscription = this.store.select('customer')
    .pipe(
      map(
        customerStoreState =>{
          // console.log(customerStoreState);
          return customerStoreState.customers;
        }
      )
    ).subscribe(
      customers=>{
        this.customerList = customers;
        console.log(this.customerList);
      }
    )
  }


  ngOnDestroy(){
    if(this.storeSubscription){
      this.storeSubscription.unsubscribe();
    }
  }
}
