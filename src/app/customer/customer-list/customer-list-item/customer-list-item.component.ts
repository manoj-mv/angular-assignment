import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { CustomerCommunicationService } from '../../customer-communication.service';
import { Customer } from '../../model/customer.model';

import * as fromApp from '../../../store/app.reducer';
import * as customerActions from '../../store/customer.actions';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-customer-list-item',
  templateUrl: './customer-list-item.component.html',
  styleUrls: ['./customer-list-item.component.scss']
})
export class CustomerListItemComponent implements OnInit {
  @Input('customerData') customer:Customer;
  @Input('customerId') index:number;
  storeSubscription:Subscription;
  constructor(
    private customerCommunService:CustomerCommunicationService,
    private store:Store<fromApp.AppState>,
    private router:Router
  ) { }

  ngOnInit(): void {
  }

  onViewClick(index:number){
    console.log(index);
    let selectedUser;
    //get selected customer using service
    selectedUser = this.customerCommunService.getCustomerFromIndex(index);
    // dispatch action
    if(selectedUser){
      this.store.dispatch(new customerActions.SelectedCustomer({user:selectedUser,id:index,showPage:'view'}))
    }
  }

}
