import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Customer } from '../model/customer.model';

import * as fromApp from '../../store/app.reducer';
import * as customerActions from '../store/customer.actions';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

export interface country{
  id:number;
  name:string;
  isselected:boolean
}

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.scss']
})
export class CustomerFormComponent implements OnInit,OnDestroy {
  customerForm : FormGroup;
  storeSub :Subscription;
  selectedCustomer:Customer;
  selectedCustomerId:number;
  editMode:boolean;
  country_list: country[] = [
    {id:0,name:'India',isselected:false},
    {id:1,name:'China',isselected:false},
    {id:2,name:'Nepal',isselected:false},
  ]
  constructor(
    private store:Store<fromApp.AppState>,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.storeSub = this.store.select('customer')
      .subscribe(
        customerState =>{
          // console.log(customerState);
          // if(customerState.selectedCustomer){
          //   this.selectedCustomer = customerState.selectedCustomer;
          //   this.editMode = customerState.editMode;
          //   this.selectedCustomerId = customerState.selectedCustomerId;
          //   if(customerState.editMode){
          //     this.initForm();
          //   }
          // }
          this.selectedCustomer = customerState.selectedCustomer;
          this.editMode = customerState.editMode;
          this.selectedCustomerId = customerState.selectedCustomerId;
          this.initForm();
        }
      );
  }

  private initForm(){
    console.log('inside initForm');
    let fname='';
    let lname='';
    let accountNum='';
    let email= '';
    let password='';
    let state='';
    let gender='';
    // let country:country[] =new FormArray(this.country_list);
    if(this.editMode && this.selectedCustomer){
      console.log('editmode');
      fname = this.selectedCustomer.fname;
      lname = this.selectedCustomer.lname;
      accountNum = this.selectedCustomer.accountNum;
      email = this.selectedCustomer.email;
      password =this.selectedCustomer.password;
      state =this.selectedCustomer.state;
      gender = this.selectedCustomer.gender;
      // country = this.selectedCustomer.country;
    }
    this.customerForm = new FormGroup(
      {
        'fname': new FormControl(fname, [Validators.required,Validators.maxLength(16),Validators.pattern('^[a-zA-Z ]+[0-9]*[a-zA-Z ]*$')]),
        'lname': new FormControl(lname, [Validators.required,Validators.maxLength(16),Validators.pattern('^[a-zA-Z ]+[0-9]*[a-zA-Z ]*$')]),
        'accountNum': new FormControl(accountNum, [Validators.required,Validators.maxLength(12)]),
        'email': new FormControl(email,[Validators.required,Validators.email]),
        'password': new FormControl(password,[Validators.required]),
        'state': new FormControl(state,[Validators.required]),
        'gender': new FormControl(gender,[Validators.required]),
        // 'country': new FormControl(country),
        
      }
    );
    console.log(this.customerForm.value);
  }


  onSubmit(){
    console.log(this.customerForm)
    if(this.editMode === true){
      console.log('edit dispatch')
      this.store.dispatch(new customerActions.UpdateCustomer({index:this.selectedCustomerId,editedCustomer:this.customerForm.value}))
    }
    else{
      console.log('add action dispatch');
      this.store.dispatch(new customerActions.AddCustomer({newCustomer:this.customerForm.value}))
    }
  }

  onCancel(){
    if(this.editMode){
      this.router.navigate(['/dashboard'])
    }
    else{
      this.store.dispatch(new customerActions.GoToDefault());
    }
    
  }
  ngOnDestroy(){
    if(this.storeSub){
      this.storeSub.unsubscribe();
    }
  }
}

