import { Component, DoCheck, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CustomerCommunicationService } from '../customer-communication.service';

import * as fromApp from '../../store/app.reducer';
import { Store } from '@ngrx/store';
import { map, take } from 'rxjs/operators';
import { Customer } from '../model/customer.model';
import * as customerActions from '../store/customer.actions';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})
export class CustomerDetailComponent implements OnInit,OnDestroy {
  communicationSub:Subscription;
  storeSub:Subscription;
  index:number;
  loadedFeature:string = 'default';
  selectedCustomer : Customer;
  
  constructor(
    private customerCommunService:CustomerCommunicationService,
    private store:Store<fromApp.AppState>
  ) { }

  ngOnInit(): void {
    // this.communicationSub = this.customerCommunService.viewClickSub.subscribe(
    //   data=>{
    //     console.log(data);
    //     this.index = data.id;
    //     this.event =data.event;
    //     console.log('detail',this.index,this.event);
    //   }
    // )

    // this.storeAccessSub = this.store.select('customer').pipe(
    //   map(
    //     customerState =>{
    //       return customerState.customers;
    //     }
    //   )
    // ).subscribe(
    //   customers=>{
    //     console.log(customers,this.index);
    //     this.currentCustomer = customers[this.index];
    //     console.log('wefwf',this.currentCustomer);
    //   }
    // )
    this.storeSub = this.store.select('customer')
      .subscribe(
        customerState =>{
          // console.log(customerState);
          if(customerState.selectedCustomer && customerState.showOnSelect === 'view'){
            this.selectedCustomer = customerState.selectedCustomer;
            if(customerState.selectedCustomerId === null){
              //  console.log('nw index',customerState.customers.indexOf(this.selectedCustomer));
              this.index = customerState.customers.indexOf(this.selectedCustomer);
            }
            else{
              this.index = customerState.selectedCustomerId;
            }
            this.loadedFeature = customerState.showOnSelect;
          }
          // else{
          //   this.selectedCustomer = null;
          //   this.index = null;
          //   this.loadedFeature = 'default';
          // }
          
        }
      );
  }


  onAddClick(){
    console.log('add button clicked')
    this.store.dispatch(new customerActions.AddFormView({showPage:'edit'}))
    // this.store.dispatch(new customerActions.SelectedCustomer({user:null,id:null,showPage:'edit'}))
  }

  onEditClick(index:number){
    // const selectedUser = this.customerCommunService.getCustomerFromIndex(index);
    console.log(this.selectedCustomer);
    if(this.selectedCustomer){
      this.store.dispatch(new customerActions.SelectedCustomer({user:this.selectedCustomer,id:index,showPage:'edit'}))
    }
  }

  onDeleteClick(index:number){ 
    this.store.dispatch(new customerActions.DeleteCustomer({index:index}));
    // this.router.navigate(['/dashboard']);
  }
  ngOnDestroy(){
    if(this.communicationSub){
      this.communicationSub.unsubscribe();
    }

    if(this.storeSub){
      this.storeSub.unsubscribe();
    }
  }

}
