import { ActionReducerMap } from '@ngrx/store';
import * as fromCustomer from '../customer/store/customer.reducer';
import * as fromAuthentication from '../authentication/store/authentication.reducer';

export interface AppState{
    customer: fromCustomer.CustomerState;
    authentication: fromAuthentication.authState;
}

export const appReducer: ActionReducerMap<AppState> = {
    customer : fromCustomer.CustomerReducer,
    authentication: fromAuthentication.AuthenticationReducer
}